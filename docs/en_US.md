# lateralwithgallery menu

===================================

![demo-popover](https://bitbucket.org/koapp/koapp-module-lateralwithgallerymenu/raw/8da98d0ce0d21df8c75218f7b78f9940b265ded3/images/popover.png)

### Description

A menu that includes configurable elements in addition to an image gallery.

### Configuration

In this menu you just have to fill in the field enabled with the URL of said images that you want to enter, in addition to the title it is also modifiable (to your liking). 
We take care that the images appear on the screen by taking the responsive parameters of the web.

### Details:

-   Author: King of app
-   Version: 0.0.1