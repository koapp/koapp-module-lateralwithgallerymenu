# lateralwithgallery menu

===================================

![demo-popover](https://bitbucket.org/koapp/koapp-module-lateralwithgallerymenu/raw/8da98d0ce0d21df8c75218f7b78f9940b265ded3/images/popover.png)

### Descripción:

Un menú que incluye elementos configurables además de una galería de imágenes.

### Configuración:

En este menú solo hay que rellenar el campo habilitado con la URL de dichas imágenes que quieras introducir, además del título también es modificable (a su gusto).
Nosotros nos encargamos de que salga en la pantalla las imágenes cogiendo los parámetros responsive de la web.

### Detalles:

-   Autor: King of app
-   Versión: 0.0.1